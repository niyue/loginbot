// ==UserScript==
// @name           12306loginbot
// @namespace      12306.cn
// @include        https://dynamic.12306.cn/* 
// @description    A login robot for 12306.cn
// @version        0.0.1
// @author         ny
// @homepage       http://example.com
// @run-at         document-end
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js
// // ==/UserScript==

function contains(data, target) {
    return data.indexOf(target) >= 0;
}

function timestamp() {
    var now = new Date();
    var timestamp = "[" + now.getHours() + "-" + now.getMinutes() + "-" + now.getSeconds() + "]";
    return timestamp;
}

function trace(log) {
    $('#loginlogs').append('<li>' + timestamp() + log + '</li>');
}

function loginbot() {
    var loginUri = "https://dynamic.12306.cn/otsweb/loginAction.do?method=login";
    var loginForm = $('#loginForm').serialize();
    var i = 0;
    for(i=0;i<5;i++) {
    $.post(loginUri, loginForm, function(data) {
        var wrongCaptcha = "请输入正确的验证码";
        var myorder = "我的订单";
        if(contains(data, wrongCaptcha)) {
            trace('please enter correct CAPTCHA code');
        }
        else if(contains(data, myorder)) {
            window.location.href = "https://dynamic.12306.cn/otsweb/order/querySingleAction.do?method=init";
        }
        else {
            trace('fail to login to server');
        }
    });
    }
}

$(document).ready(function() {
    var loginForm = $('#loginForm');
    loginForm.append('<input type="button" value="Loginbot" id="loginbotbutton"/>');
    loginForm.append('<ul id="loginlogs" style="float:right;position:absolute;"></ul>');
    $('#loginbotbutton').click("click", loginbot);
});